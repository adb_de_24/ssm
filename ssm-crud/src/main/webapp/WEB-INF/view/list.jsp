<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户列表</title>
<%
	pageContext.setAttribute("basepath", request.getContextPath());
%>
<script type="text/javascript"
	src="${basepath}/static/js/jquery-2.1.1.min.js"></script>
<link href="${basepath}/static/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" />
<script src="${basepath}/static/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript">
	$(function() {
		//全选和全不选功能
		 $("#checkAll").click(function() {
			$(".check_item").prop("checked",$(this).prop("checked"));
			$(document).on("click",".check_item",function(){
				var falg = $(".check_item:checked").length == $(".check_item").length;
				$("#checkAll").prop("checked",falg);
			});
		}); 

		$('#delAll').click(function() {
			var check_boxes = $("input[name='id']:checked").serialize();
			if (check_boxes.length <= 0) {
				alert('您未勾选，请勾选！');
				return;
			}
			if (confirm('您确定要删除吗？')) {
				var dropIds = new Array();
				$("input[name='id']:checked").each(function(){
					dropIds.push($(this).val());
				});
				$.ajax({
					type : 'post',
					traditional : true,
					url : '${basepath}/batchDelUser',
					data : {'dropIds' : dropIds},
					success : function(data) {
						if (data.code == 100) {
							alert('删除成功！');
						}else if (data.code == 200) {
							alert('删除失败！');
						}
					}
				});
			}
		});
		
		
		//点击新增按钮弹出模态框
		$("#user_add_modal_btn").click(function() {
			//员工下拉列表
			getDepts("#userAddModal select");
			
			//弹出模态框
			$("#userAddModal").modal({
				backdrop : "static"
			});
		});
		
		//校验表单数据
		function validate_add_form(){
			//1.拿到要校验的数据，使用正则表达式
			var name = $("#name").val();
			var regName = /(^[a-zA-Z0-9_-]{6,16})|(^[\u2E80-\u9FFF]{2,5})$/
			if(!regName.test(name)){
				//alert("用户名必须是2-5位中文或者6-16位英文和数字的组合！")
				show_validate_msg("#name","has-error","用户名必须是2-5位中文或者6-16位英文和数字的组合")
				return false;
			}else{
				show_validate_msg("#name","has-success","")
			}
			
			var password = $("#password").val();
			var regPwd = /^[a-zA-Z0-9_-]{6,18}$/
			if(!regPwd.test(password)){
				//alert("密码必须是6-18位英文和数字的组合！")
				show_validate_msg("#password","has-error","密码必须是6-18位英文和数字的组合！")
				return false;
			}else{
				show_validate_msg("#password","has-success","")
			}
			
			var age = $("#age").val();
			var regage = /^1[89]|[2-5]\d$/
			if(!regage.test(age)){
				show_validate_msg("#age","has-error","童鞋，你未满18岁啊！")
				return false;
			}else{
				show_validate_msg("#age","has-success","")
			}
			return true;
		}
		
		//提示错误信息
		function show_validate_msg(ele,status,msg){
			//清除当前元素的校验状态
			$(ele).parent().removeClass("has-success has-error");
			$(ele).next("span").text("")
			
			if("has-success" == status){
				$(ele).parent().addClass(status)
				$(ele).next("span").text("")
			}else if("has-error" == status){
				$(ele).parent().addClass(status)
				$(ele).next("span").text(msg)
			}
		}
		
		//保存
		$("#user_save_btn").click(function(){
			//1.模态框填写的数据提交给服务器保存
			
			//2.先对要提交给服务器的的数据进行校验
			if(!validate_add_form()){
				return false;
			}
			//发送ajax保存用户信息
			 $.ajax({
				url:"${basepath}/saveUser",
				type:"POST",
				data:$("#userAddModal form").serialize(),
				success:function(result){
					//保存用户成功
					//1.关闭
					$("#userAddModal").modal('hide');
					//2.去到末页
					to_page(totalReard);
					if(result.code == 100){
						alert(result.msg);
					}else{
						alert(result.msg);
					}
					location.reload([true]); 
				}
			});
		});
		
		//更新
		$("#user_edit_btn").click(function(){
			//发送ajax保存用户信息
			 $.ajax({
				url:"${basepath}/updateUser",
				type:"POST",
				data:$("#userEditModal form").serialize(),
				success:function(result){
					//保存用户成功
					//1.关闭
					$("#userEditModal").modal('hide');
					//2.去到末页
					to_page(totalReard);
					if(result.code == 100){
						alert(result.msg);
					}else{
						alert(result.msg);
					}
					//刷新页面
					location.replace(document.referrer);
				}
			});
			
		});
		
		function getDepts(ele){
			//清空之前下拉的值
			$(ele).empty();
			 //发送ajax请求查询部门信息，显示在下拉框
			$.ajax({
				url:"${basepath}/queryDepts",
				type:"GET",
				success:function(result){
					//{"code":"100","msg":"处理成功!",
						//"extned":{"depts":[{"deptNo":1,"deptName":"开发部"},{"deptNo":2,"deptName":"测试部"},{"deptNo":3,"deptName":"运营部"},{"deptNo":4,"deptName":"风控部"},{"deptNo":5,"deptName":"人事部"}]}}
					//console.log(result);
					$.each(result.extned.depts,function(){
						var optionEle = $("<option></option>").append(this.deptName).attr("value",this.deptNo);
						optionEle.appendTo(ele);
					});
				}
			});
		}
		
		function getUser(id){
			$.ajax({
				url:'${basepath}/queryOneUser/'+id,
				type:'GET',
				success:function(result){
					/* console.info(result); */
					var userData = result.extned.user;
					$("#id").val(userData.id);
					$("#edit_name").val(userData.name);
					$("#edit_password").val(userData.password);
					$("#edit_age").val(userData.age);
					$("#userEditModal input[name=sex]").val([userData.sex]);
					$("#userEditModal select").val([userData.deptId]);
				}
			});
		}
		
		//编辑单个用户
		$(document).on("click","#editOneUser",function(){
			//员工下拉列表
			getDepts("#userEditModal select");
			
			//2.查询用户信息
			getUser($(this).attr("edit_id"));//获取自定义属性中的id
			
			//弹出模态框
			$("#userEditModal").modal({
				backdrop : "static"
			});
		});
		
		$(document).on("click","#delOneUser",function(){
			//删除单个用户
			var userId = $(this).attr("dell_id");
			if (confirm('您确定要删除吗？')) {
				$.ajax({
					type : 'post',
					url : '${basepath}/oneDelUser',
					data : {'id' : userId},
					success : function(data) {
						if (data.code == 100) {
							alert('删除成功！');
						}else if (data.code == 200) {
							alert('删除失败！');
						}
					}
				});
			}
		});
		
	});
</script>
</head>
<body>

	<!-- 用户增加模态框 -->
	<div class="modal fade" id="userAddModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">用户增加</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal">
						<div class="form-group">
							<label for="name" class="col-sm-2 control-label">用户名</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="name" name="name"
									placeholder="输入用户名">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label for="password" class="col-sm-2 control-label">密码</label>
							<div class="col-sm-10">
								<input type="password" class="form-control" id="password" name="password"
									placeholder="输入密码">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label for="age" class="col-sm-2 control-label">年龄</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="age" name="age"
									placeholder="输入年龄">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label for="sex" class="col-sm-2 control-label">性别</label>
							<div class="col-sm-10">
								<label class="radio-inline"> <input type="radio"
									name="sex" id="inlineRadio1" value="M" checked="checked">男
								</label> <label class="radio-inline"> <input type="radio"
									name="sex" id="inlineRadio2" value="F">女
								</label>
							</div>
						</div>
						<div class="form-group">
							<label for="deptId" class="col-sm-2 control-label">部门</label>
							<div class="col-sm-4">
								<select class="form-control" name="deptId">
								</select>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" id="user_save_btn">保存</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- 用户修改模态框 -->
	<div class="modal fade" id="userEditModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" >用户修改</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal">
						<input type="hidden" class="form-control" id="id" name="id">
						<div class="form-group">
							<label for="name" class="col-sm-2 control-label">用户名</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="edit_name" name="name"
									placeholder="输入用户名">
							</div>
						</div>
						<div class="form-group">
							<label for="password" class="col-sm-2 control-label">密码</label>
							<div class="col-sm-10">
								<input type="password" class="form-control" id="edit_password" name="password"
									placeholder="输入密码">
							</div>
						</div>
						<div class="form-group">
							<label for="age" class="col-sm-2 control-label">年龄</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="edit_age" name="age"
									placeholder="输入年龄">
							</div>
						</div>
						<div class="form-group">
							<label for="sex" class="col-sm-2 control-label">性别</label>
							<div class="col-sm-10">
								<label class="radio-inline"> <input type="radio"
									name="sex" id="inlineRadio1" value="M" checked="checked">男
								</label> <label class="radio-inline"> <input type="radio"
									name="sex" id="inlineRadio2" value="F">女
								</label>
							</div>
						</div>
						<div class="form-group">
							<label for="deptId" class="col-sm-2 control-label">部门</label>
							<div class="col-sm-4">
								<select class="form-control" name="deptId">
								</select>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" id="user_edit_btn">保存</button>
				</div>
			</div>
		</div>
	</div>

	<div class="container ">
		<!-- 标题 -->
		<div class="row">
			<div class="col-md-6">
				<h1>SSM-CRUD</h1>
			</div>
		</div>
		<!-- 按钮 -->
		<div class="row">
			<div class="col-md-4 col-md-offset-10">
				<button class="btn btn-primary" id="user_add_modal_btn">新增用户</button>
				<button class="btn btn-danger " id="delAll">批量删除</button>
			</div>
		</div>
		<br>
		<!-- 表格数据-->
		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered">
					<tr>
						<th><input type="checkbox" id="checkAll" /></th>
						<th>编号</th>
						<th>用户名</th>
						<th>年龄</th>
						<th>积分</th>
						<th>性别</th>
						<th>部门</th>
						<th>操作</th>
					</tr>
					<c:forEach items="${PageInfo.list}" var="user" varStatus="status">
						<tr
							<c:if test="${status.index % 2 != 0}">style='background-color:#ECF6EE;'</c:if>>
							<td><input type="checkbox" class="check_item" name="id" value="${user.id}" /></td>
							<td>${status.index+1}</td>
							<td>${user.name}</td>
							<td>${user.age}</td>
							<td>${user.roleid}</td>
							<td>${user.sex=="M"?"男":"女"}</td>
							<td>${user.dept.deptName}</td>
							<td>
								<button type="button" class="btn btn-primary btn-xs" id="editOneUser" edit_id="${user.id}">
									<span class="glyphicon glyphicon-pencil"></span> 编辑
								</button>
								<button type="button" class="btn btn-danger btn-xs" id="delOneUser" dell_id="${user.id}">
									<span class="glyphicon glyphicon-trash"></span> 删除
								</button>
							</td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
		<!-- 分页 -->
		<div class="row">
			<div class="col-md-6">当前第${PageInfo.pageNum}页;总${PageInfo.pages}页;共${PageInfo.total}条记录</div>
			<div class="col-md-6">
				<nav aria-label="Page navigation">
				<ul class="pagination">
					<li><a href="${basepath}/queryUser?pn=1">首页</a></li>
					<c:if test="${PageInfo.hasPreviousPage }">
						<li><a href="${basepath}/queryUser?pn=${PageInfo.pageNum-1}"
							aria-label="Previous"> <span aria-hidden="true">&laquo;</span></a>
						</li>
					</c:if>
					<c:forEach items="${PageInfo.navigatepageNums}" var="pagenum">
						<c:if test="${pagenum == PageInfo.pageNum }">
							<li class="active"><a
								href="${basepath}/queryUser?pn=${pagenum}">${pagenum}</a></li>
						</c:if>
						<c:if test="${pagenum != PageInfo.pageNum }">
							<li><a href="${basepath}/queryUser?pn=${pagenum}">${pagenum}</a></li>
						</c:if>
					</c:forEach>
					<c:if test="${PageInfo.hasNextPage }">
						<li><a href="${basepath}/queryUser?pn=${PageInfo.pageNum+1}"
							aria-label="Next"> <span aria-hidden="true">&raquo;</span></a></li>
					</c:if>
					<li><a href="${basepath}/queryUser?pn=${PageInfo.pages}">尾页</a></li>
				</ul>
				</nav>
			</div>
		</div>
	</div>
	
	
</body>
</html>