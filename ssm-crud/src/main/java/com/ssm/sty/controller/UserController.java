package com.ssm.sty.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.sty.bean.User;
import com.ssm.sty.entity.Message;
import com.ssm.sty.service.UserService;

/**
 * 用户控制层
 * @author Qiurz
 *
 */
@Controller
public class UserController {
	
	@Autowired
	UserService userService;

	/**
	 * 查询用户列表
	 * @param pn
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/queryUser")
	public String queryUser(@RequestParam(value="pn",defaultValue="1")Integer pn, Model model) {
		//引入分页插件pagehelper
		PageHelper.startPage(pn, 5);
		List<User> users = userService.queryAllUser();
		PageInfo page = new PageInfo(users,5);
		model.addAttribute("PageInfo", page);
		return "list";
	}
	
//	@RequestMapping(value="/queryUser")
//	public @ResponseBody Message queryUserWithJson(@RequestParam(value="pn",defaultValue="1")Integer pn, Model model) {
//		//引入分页插件pagehelper
//		PageHelper.startPage(pn, 5);
//		List<User> users = userService.queryAllUser();
//		PageInfo page = new PageInfo(users,5);
//		return new Message().success().add("PageInfo", page);
//	}
	
	/**
	 * 新增用户
	 * @param user
	 * @return
	 */
	@RequestMapping(value="/saveUser",method=RequestMethod.POST)
	public @ResponseBody Message saveUser(User user){
		user.setRoleid(5);//用户注册获赠5积分
		userService.save(user);
		return Message.success();
	}
	
	/**
	 * 修改用户
	 * @param user
	 * @return
	 */
	@RequestMapping(value="/updateUser",method=RequestMethod.POST)
	public @ResponseBody Message updateUser(User user){
		user.setRoleid(5);//用户注册获赠5积分
		int i = userService.update(user);
		if (i <= 0) {
			return Message.faile();
		}
		return Message.success();
	}
	
	/**
	 * 查询单个用户
	 * @param user
	 * @return
	 */
	@RequestMapping(value="/queryOneUser/{id}",method=RequestMethod.GET)
	public @ResponseBody Message queryOneUser(@PathVariable("id") Integer id){
		User user = userService.queryOneUser(id);
		return Message.success().add("user", user);
	}
	
	/**
	 * 单个删除
	 * @param user
	 * @return
	 */
	@RequestMapping(value="/oneDelUser",method=RequestMethod.POST)
	public @ResponseBody Message oneDelUser(User user){
		try {
			int i = userService.oneDelUser(user);
			if (i <= 0) {
				return Message.faile();
			}
		} catch (Exception e) {
			return Message.faile();
		}
		return Message.success();
	}
	
	/**
	 * 批量删除
	 * @param user
	 * @return
	 */
	@RequestMapping(value="/batchDelUser",method=RequestMethod.POST)
	public @ResponseBody Message batchDelUser(User user){
		try {
			int i = userService.batchDelUser(user);
			if (i != 1) {
				return Message.faile();
			}
		} catch (Exception e) {
			return Message.faile();
		}
		return Message.success();
	}
}
