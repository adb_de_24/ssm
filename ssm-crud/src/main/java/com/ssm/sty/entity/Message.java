package com.ssm.sty.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * 通用的类
 * @author Qiurz
 *
 */
public class Message {

	/**
	 * 返回码   100：成功   200：失败
	 */
	private String code;
	/**
	 * 状态描述
	 */
	private String msg;
	/**
	 * 数据
	 */
	public Map<String, Object> extned = new HashMap<String, Object>();
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Map<String, Object> getExtned() {
		return extned;
	}
	public void setExtned(Map<String, Object> extned) {
		this.extned = extned;
	}
	
	public static Message success(){
		Message message = new Message();
		message.setCode("100");
		message.setMsg("处理成功!");
		return message;
	}
	
	public static Message faile(){
		Message message = new Message();
		message.setCode("200");
		message.setMsg("处理失败!");
		return message;
	}
	
	public Message add(String key,Object object){
		this.getExtned().put(key, object);
		return this;
	}
}
