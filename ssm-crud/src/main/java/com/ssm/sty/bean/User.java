package com.ssm.sty.bean;

import java.util.List;

public class User {
    private Integer id;
    private String name;
    private Integer age;
    private String password;
    private Integer roleid;
    private String sex;
    private Integer deptId;
    private Dept dept;
    private List<Integer> dropIds;
    
    
    public List<Integer> getDropIds() {
		return dropIds;
	}
	public void setDropIds(List<Integer> dropIds) {
		this.dropIds = dropIds;
	}
	public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }
    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }
    public Integer getRoleid() {
        return roleid;
    }
    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }
    public String getSex() {
        return sex;
    }
    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }
   
	public Integer getDeptId() {
		return deptId;
	}
	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}
	public Dept getDept() {
		return dept;
	}
	public void setDept(Dept dept) {
		this.dept = dept;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", age=" + age + ", roleid=" + roleid + ", sex=" + sex
				+ ", deptId=" + deptId + ", dept=" + dept + ", dropIds=" + dropIds + "]";
	}

}