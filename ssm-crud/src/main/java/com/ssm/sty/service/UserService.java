package com.ssm.sty.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssm.sty.bean.User;
import com.ssm.sty.dao.UserMapper;

@Service
public class UserService {

	@Autowired
	UserMapper userMapper;
	
	/**
	 * 查询用户
	 * @return
	 */
	public List<User> queryAllUser() {
		return userMapper.selectByExampleWithDept(null);
	}

	/**
	 * 保存用户
	 * @param user
	 */
	public void save(User user) {
		userMapper.insertSelective(user);
	}
	
	/**
	 * 删除单个用户
	 * @param user
	 * @return
	 */
	public int oneDelUser(User user) {
		return userMapper.oneDelUser(user);
	}

	/**
	 * 批量用户
	 * @param user
	 */
	public int batchDelUser(User user) {
		return userMapper.batchDelUser(user);
	}

	/**
	 * 查询单个用户
	 * @param id
	 * @return
	 */
	public User queryOneUser(Integer id) {
		return userMapper.selectByPrimaryKey(id);
	}

	/**
	 * 更新用户
	 * @param user
	 */
	public int update(User user) {
		return userMapper.updateByPrimaryKey(user);
	}
	
}
