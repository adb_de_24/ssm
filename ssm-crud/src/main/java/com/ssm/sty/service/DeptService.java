package com.ssm.sty.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssm.sty.bean.Dept;
import com.ssm.sty.dao.DeptMapper;

@Service
public class DeptService {
	
	@Autowired
	private DeptMapper deptMapper;

	public List<Dept> getDepts() {
		// TODO Auto-generated method stub
		return deptMapper.selectByExample(null);
	}

	
}
