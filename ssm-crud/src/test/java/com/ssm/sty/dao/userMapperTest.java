package com.ssm.sty.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 测试dao层
 * @author Qiurz
 *1.导入springtest模块
 *2.@ContextConfiguration指定我们需要的组件
 *3.直接autowired导入
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationContext.xml"})
public class userMapperTest {
	
	@Autowired
	private UserMapper userMapper;

	@Test
	public void test() {
		System.out.println(userMapper);
	}

}
