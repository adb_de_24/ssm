/*
Navicat MySQL Data Transfer

Source Server         : local-mysql
Source Server Version : 50718
Source Host           : 127.0.0.1:3306
Source Database       : ssm-crud

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2017-10-03 12:09:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_no` int(10) NOT NULL,
  `dept_name` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('1', '技术部');
INSERT INTO `sys_dept` VALUES ('2', '开发部');
INSERT INTO `sys_dept` VALUES ('3', '人事部');
INSERT INTO `sys_dept` VALUES ('4', '行政部');
INSERT INTO `sys_dept` VALUES ('5', '运营部');
INSERT INTO `sys_dept` VALUES ('5', '风控部');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `age` int(10) DEFAULT NULL,
  `roleid` int(10) DEFAULT NULL,
  `sex` varchar(4) DEFAULT NULL,
  `dept_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '111111', '12', '10', 'M', '1');
INSERT INTO `sys_user` VALUES ('2', '汤姆', '123456', '23', '5', 'F', '3');
INSERT INTO `sys_user` VALUES ('3', '杰克', '123456', '43', '5', 'M', '2');
INSERT INTO `sys_user` VALUES ('4', '刘畅', '112', '24', '5', 'F', '4');
INSERT INTO `sys_user` VALUES ('11', '关羽', '123456', '12', '5', 'F', '3');
INSERT INTO `sys_user` VALUES ('13', '张飞', '123456', '25', '5', 'F', '5');
