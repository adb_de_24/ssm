<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>用户列表</title>
<%
	pageContext.setAttribute("basepath", request.getContextPath());
%>
<script type="text/javascript"
	src="${basepath}/static/js/jquery-2.1.1.min.js"></script>
<link href="${basepath}/static/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" />
<script src="${basepath}/static/bootstrap/js/bootstrap.min.js"></script>
<style type="text/css">
.table th {
	text-align: center;
}
/* .table td { 
	text-align: center; 
} */
</style>
<script type="text/javascript">
	var totalReard;
	$(function() {
		//去首页
		to_page(1);
	})

	function to_page(pn) {
		$.ajax({
			url : "${basepath}/queryUser",
			data : "pn=" + pn,
			type : "GET",
			success : function(result) {
				//1.解析并显示用户json数据
				build_users_table(result);
				//2.解析并显示分页信息数据
				build_page_info(result);
				//3.解析并显示分页条数据
				build_page_nav(result);
			}
		});
	}

	function build_users_table(result) {
		//清空
		$("#users_table tbody").empty();
		var users = result.data.PageInfo.list;
		$.each(users, function(index, item) {
			var userId = $("<td></td>").append(item.id);
			var userName = $("<td></td>").append(item.name);
			var userPwd = $("<td></td>").append(item.password);
			var userAge = $("<td></td>").append(item.age);
			var userRoleid = $("<td></td>").append(item.roleid);
			var userSex = $("<td></td>").append(item.sex == "M" ? "男" : "女");
			var userDept = $("<td></td>").append(item.dept.deptName);
			var editBtn = $("<button></button>").addClass(
					"btn btn-primary btn-xs").append(
					$("<span></span>").addClass("glyphicon glyphicon-pencil"))
					.append("编辑");
			var delBtn = $("<button></button>").addClass(
					"btn btn-danger btn-xs").append(
					$("<span></span>").addClass("glyphicon glyphicon-trash"))
					.append("删除");
			var tdBtn = $("<td></td>").append(editBtn).append(delBtn);
			$("<tr></tr>").append(userId).append(userName).append(userPwd)
					.append(userAge).append(userRoleid).append(userSex).append(
							userDept).append(tdBtn).appendTo(
							"#users_table tbody");
		});
	}

	function build_page_info(result) {
		//清空
		$("#page_info_nav").empty();
		$("#page_info_nav").append(
				"当前第" + result.data.PageInfo.pageNum + "页;总"
						+ result.data.PageInfo.pages + "页;共"
						+ result.data.PageInfo.total + "条记录");
		totalReard = result.data.PageInfo.total;
	}

	function build_page_nav(result) {
		//清空
		$("#page_nav_area").empty();
		var ul = $("<ul></ul>").addClass("pagination")
		var firstPageLi = $("<li></li>").append(
				$("<a></a>").append("首页").attr("href", "#"));
		var prePageLi = $("<li></li>").append(
				$("<a></a>").append("&laquo;").attr("href", "#"));
		if (result.data.PageInfo.hasPreviousPage == false) {
			firstPageLi.addClass("disabled");
			prePageLi.addClass("disabled");
		} else {
			firstPageLi.click(function() {
				//去首页
				to_page(1);
			});
			prePageLi.click(function() {
				//上一页
				to_page(result.data.PageInfo.pageNum - 1);
			});
		}
		var nextPageLi = $("<li></li>").append(
				$("<a></a>").append("&raquo;").attr("href", "#"));
		var lastPageLi = $("<li></li>").append(
				$("<a></a>").append("尾页").attr("href", "#"));
		if (result.data.PageInfo.hasNextPage == false) {
			nextPageLi.addClass("disabled");
			lastPageLi.addClass("disabled");
		} else {
			nextPageLi.click(function() {
				//下一页
				to_page(result.data.PageInfo.pageNum + 1);
			});
			lastPageLi.click(function() {
				//尾页
				to_page(result.data.PageInfo.pages);
			});
		}
		ul.append(firstPageLi).append(prePageLi);
		$.each(result.data.PageInfo.navigatepageNums, function(index, item) {
			var numLi = $("<li></li>").append(
					$("<a></a>").append(item).attr("href", "#"));
			if (result.data.PageInfo.pageNum == item) {
				numLi.addClass("active");
			}
			numLi.click(function() {
				to_page(item);
			});
			ul.append(numLi);
		});
		ul.append(nextPageLi).append(lastPageLi);
		var navEle = $("<nav></nav>").append(ul);
		navEle.appendTo("#page_nav_area");
	}

	//点击新增按钮弹出模态框
	$("#user_add_modal_btn").click(
			function() {
				//发送ajax请求查询部门信息，显示在下拉框
				$.ajax({
					url : "${basepath}/queryDepts",
					type : "GET",
					success : function(result) {
						//{"code":"100","msg":"处理成功!",
						//"data":{"depts":[{"deptNo":1,"deptName":"开发部"},{"deptNo":2,"deptName":"测试部"},{"deptNo":3,"deptName":"运营部"},{"deptNo":4,"deptName":"风控部"},{"deptNo":5,"deptName":"人事部"}]}}
						//console.log(result);
						$.each(result.data.depts, function() {
							var optionEle = $("<option></option>").append(
									this.deptName).attr("value", this.deptNo);
							optionEle.appendTo("#userAddModal select");
						});
					}
				});

				//弹出模态框
				$("#userAddModal").modal({
					backdrop : "static"
				});
			});

	//保存
	$("#user_save_btn").click(function() {
		//发送ajax保存用户信息
		$.ajax({
			url : "${basepath}/saveUser",
			type : "POST",
			data : $("#userAddModal form").serialize(),
			success : function(result) {
				//保存用户成功
				//1.关闭
				$("#userAddModal").modal('hide');
				//2.去到末页
				to_page(totalReard);
			}
		});
	});
</script>
</head>
<body>

	<!-- 用户增加模态框 -->
	<div class="modal fade" id="userAddModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">用户增加</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal">
						<div class="form-group">
							<label for="name" class="col-sm-2 control-label">用户名</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="name" name="name"
									placeholder="输入用户名">
							</div>
						</div>
						<div class="form-group">
							<label for="password" class="col-sm-2 control-label">密码</label>
							<div class="col-sm-10">
								<input type="password" class="form-control" id="password"
									name="password" placeholder="输入密码">
							</div>
						</div>
						<div class="form-group">
							<label for="age" class="col-sm-2 control-label">年龄</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="age" name="age"
									placeholder="输入年龄">
							</div>
						</div>
						<div class="form-group">
							<label for="sex" class="col-sm-2 control-label">性别</label>
							<div class="col-sm-10">
								<label class="radio-inline"> <input type="radio"
									name="sex" id="inlineRadio1" value="M" checked="checked">男
								</label> <label class="radio-inline"> <input type="radio"
									name="sex" id="inlineRadio2" value="F">女
								</label>
							</div>
						</div>
						<div class="form-group">
							<label for="deptId" class="col-sm-2 control-label">部门</label>
							<div class="col-sm-4">
								<select class="form-control" name="deptId">
								</select>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" id="user_save_btn">保存</button>
				</div>
			</div>
		</div>
	</div>

	<div class="container ">
		<!-- 标题 -->
		<div class="row">
			<div class="col-md-6">
				<h1>SSM-CRUD</h1>
			</div>
		</div>
		<!-- 按钮 -->
		<div class="row">
			<div class="col-md-4 col-md-offset-10">
				<button class="btn btn-primary" id="user_add_modal_btn">新增</button>
				<button class="btn btn-danger">删除</button>
			</div>
		</div>
		<br>
		<!-- 表格数据-->
		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered" id="users_table">
					<thead>
						<tr>
							<th>编号</th>
							<th>用户名</th>
							<th>密码</th>
							<th>年龄</th>
							<th>积分</th>
							<th>性别</th>
							<th>部门</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>
		<!-- 分页 -->
		<div class="row">
			<div class="col-md-6" id="page_info_nav"></div>
			<div class="col-md-6" id="page_nav_area"></div>
		</div>
	</div>
</body>
</html>