<%@ page contentType="text/html; charset=utf-8" isErrorPage="true"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:set var="basePath" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title><spring:message code="404" /></title>
<link rel="stylesheet" href="${basePath }/static/css/404/style.css"/>
<link rel="stylesheet" href="${basePath }/static/css/404/base.css"/>
</head>
<body>
	<div id="errorpage">
		<div class="tfans_error">
			<div class="logo"></div>
			<div class="errortans clearfix">
				<div class="e404"></div>
				<p>
					<b>出错啦！</b>
				</p>
				<p>您访问的页面不存在</p>
				<div class="bt">
					<a href="#">返回首页</a>
				</div>
			</div>
		</div>
	</div>

</body>
</html>