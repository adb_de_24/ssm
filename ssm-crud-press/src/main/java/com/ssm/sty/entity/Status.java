package com.ssm.sty.entity;

public enum Status {
	
	success("1000","成功"),
	fail("2000","失败");
	
	private String respCode;
	private String respDesc;
	
	private Status(String code,String desc){
		this.respCode = code;
		this.respDesc = desc;
	}

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getRespDesc() {
		return respDesc;
	}

	public void setRespDesc(String respDesc) {
		this.respDesc = respDesc;
	}
	
}
