package com.ssm.sty.entity;


/**
 * 统一返回结果类
 * @author Qiurz
 *
 */
public class Message {
	
	/**
	 * 返回码   100：成功   200：失败
	 */
	private String code;
	/**
	 * 状态描述
	 */
	private String msg;
	/**
	 * 数据结果集
	 */
	public Object data;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
}
