package com.ssm.sty.bean;

public class Dept {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_dept.dept_no
     *
     * @mbg.generated Sun Jul 23 17:42:16 CST 2017
     */
    private Integer deptNo;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_dept.dept_name
     *
     * @mbg.generated Sun Jul 23 17:42:16 CST 2017
     */
    private String deptName;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_dept.dept_no
     *
     * @return the value of sys_dept.dept_no
     *
     * @mbg.generated Sun Jul 23 17:42:16 CST 2017
     */
    public Integer getDeptNo() {
        return deptNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_dept.dept_no
     *
     * @param deptNo the value for sys_dept.dept_no
     *
     * @mbg.generated Sun Jul 23 17:42:16 CST 2017
     */
    public void setDeptNo(Integer deptNo) {
        this.deptNo = deptNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_dept.dept_name
     *
     * @return the value of sys_dept.dept_name
     *
     * @mbg.generated Sun Jul 23 17:42:16 CST 2017
     */
    public String getDeptName() {
        return deptName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_dept.dept_name
     *
     * @param deptName the value for sys_dept.dept_name
     *
     * @mbg.generated Sun Jul 23 17:42:16 CST 2017
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName == null ? null : deptName.trim();
    }

	@Override
	public String toString() {
		return "Dept [deptNo=" + deptNo + ", deptName=" + deptName + "]";
	}
    
    
}