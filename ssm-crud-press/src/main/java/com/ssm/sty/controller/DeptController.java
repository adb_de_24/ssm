package com.ssm.sty.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ssm.sty.bean.Dept;
import com.ssm.sty.entity.Message;
import com.ssm.sty.entity.Status;
import com.ssm.sty.service.DeptService;

/**
 * 查询部门信息
 * @author Qiurz
 *
 */
@Controller
public class DeptController {
	
	@Autowired
	private DeptService deptService;

	@RequestMapping("/queryDepts")
	public @ResponseBody Message queryDepts(Model model){
		Message msg = new Message();
		List<Dept> list = deptService.getDepts();
		msg.setData(list);
		msg.setCode(Status.success.getRespCode());
		msg.setMsg(Status.success.getRespDesc());
		return msg;
	}
}
