package com.ssm.sty.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.sty.bean.User;
import com.ssm.sty.common.base.BaseController;
import com.ssm.sty.entity.Message;
import com.ssm.sty.entity.Status;
import com.ssm.sty.service.UserService;

/**
 * 用户控制层
 * @author Qiurz
 */
@Controller
public class UserController extends BaseController{
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	UserService userService;

	/**
	 * 查询用户列表
	 * @param pn
	 * @param model
	 * @return
	 */
//	@RequestMapping(value="/queryUser")
//	public String queryUser(@RequestParam(value="pn",defaultValue="1")Integer pn, Model model) {
//		logger.info("开始分页：当前页为{}",pn);
//		//引入分页插件pagehelper
//		PageHelper.startPage(pn, 5);
//		List<User> users = userService.queryAllUser();
//		logger.info("分页结果：{}" , users);
//		PageInfo page = new PageInfo(users,5);
//		model.addAttribute("PageInfo", page);
//		logger.info("分页结束!");
//		return "/system/list";
//	}
	
	@RequestMapping(value="/queryUser")
	public @ResponseBody Message queryUserWithJson(
			@RequestParam(value="pn",defaultValue="1")Integer pn, Model model) {
		Message msg = new Message();
		try {
			//引入分页插件pagehelper
			PageHelper.startPage(pn, 5);
			List<User> users = userService.queryAllUser();
			PageInfo<User> PageInfo = new PageInfo<User>(users,5);
			logger.info("结果集：{}",PageInfo.getList());
			msg.setCode(Status.success.getRespCode());
			msg.setMsg(Status.success.getRespDesc());
			msg.setData(PageInfo);
		} catch (Exception e) {
			msg.setCode(Status.fail.getRespCode());
			msg.setMsg(Status.fail.getRespDesc());
		}
		return msg;
	}
	
	/**
	 * 新增用户
	 * @param user
	 * @return
	 */
	@RequestMapping(value="/saveUser",method=RequestMethod.POST)
	public @ResponseBody Message saveUser(User user){
		Message msg = new Message();
		user.setRoleid(5);//用户注册获赠5积分
		userService.save(user);
		msg.setCode(Status.success.getRespCode());
		msg.setMsg(Status.success.getRespDesc());
		return msg;
	}
	
	/**
	 * 修改用户
	 * @param user
	 * @return
	 */
	@RequestMapping(value="/updateUser",method=RequestMethod.POST)
	public @ResponseBody Message updateUser(User user){
		Message msg = new Message();
		user.setRoleid(5);//用户注册获赠5积分
		int i = userService.update(user);
		if (i <= 0) {
			msg.setCode(Status.fail.getRespCode());
			msg.setMsg(Status.fail.getRespDesc());
			return msg;
		}
		msg.setCode(Status.success.getRespCode());
		msg.setMsg(Status.success.getRespDesc());
		return msg;
	}
	
	/**
	 * 查询单个用户
	 * @param user
	 * @return
	 */
	@RequestMapping(value="/queryOneUser/{id}",method=RequestMethod.GET)
	public @ResponseBody Message queryOneUser(@PathVariable("id") Integer id){
		Message msg = new Message();
		User user = userService.queryOneUser(id);
		msg.setData(user);
		msg.setCode(Status.success.getRespCode());
		msg.setMsg(Status.success.getRespDesc());
		return msg;
	}
	
	/**
	 * 单个删除
	 * @param user
	 * @return
	 */
	@RequestMapping(value="/oneDelUser",method=RequestMethod.POST)
	public @ResponseBody Message oneDelUser(User user){
		Message msg = new Message();
		msg.setCode(Status.success.getRespCode());
		msg.setMsg(Status.success.getRespDesc());
		try {
			int i = userService.oneDelUser(user);
			if (i <= 0) {
				msg.setCode(Status.fail.getRespCode());
				msg.setMsg(Status.fail.getRespDesc());
				return msg;
			}
		} catch (Exception e) {
			msg.setCode(Status.fail.getRespCode());
			msg.setMsg(Status.fail.getRespDesc());
			return msg;
		}
		return msg;
	}
	
	/**
	 * 批量删除
	 * @param user
	 * @return
	 */
	@RequestMapping(value="/batchDelUser",method=RequestMethod.POST)
	public @ResponseBody Message batchDelUser(User user){
		Message msg = new Message();
		msg.setCode(Status.success.getRespCode());
		msg.setMsg(Status.success.getRespDesc());
		try {
			int i = userService.batchDelUser(user);
			if (i != 1) {
				msg.setCode(Status.fail.getRespCode());
				msg.setMsg(Status.fail.getRespDesc());
				return msg;
			}
		} catch (Exception e) {
			msg.setCode(Status.fail.getRespCode());
			msg.setMsg(Status.fail.getRespDesc());
			return msg;
		}
		return msg;
	}
}
