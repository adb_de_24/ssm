package com.ssm.sty.common.base;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 控制器基类
 * @author jin
 *
 */
public abstract class BaseController {

	private final static Logger logger = LoggerFactory.getLogger(BaseController.class);
	
	/**
	 * 统一异常处理
	 * @param request
	 * @param response
	 * @param exception
	 * @return
	 */
	@ExceptionHandler
	public String exceptionHandler(HttpServletRequest request,HttpServletResponse response,Exception exception) {
		logger.error("统一异常处理：",exception);
		request.setAttribute("ex", exception);
		if (null != request.getHeader("X-Requested-With") && 
				"XMLHttpRequest".equalsIgnoreCase(request.getHeader("X-Requested-With"))) {
			request.setAttribute("requestHeader", "ajax");
		}
		// shiro没有权限异常
//		if (exception instanceof UnauthorizedException) {
//			return "/403.jsp";
//		}
		// shiro会话已过期异常
//		if (exception instanceof InvalidSessionException) {
//			return "/error.jsp";
//		}
		return "/common/error";
	}
	
	/**
	 * 返回jsp视图
	 * @param path
	 * @return
	 */
	public String jsp(String path) {
		return path.concat(".jsp");
	}
	
	
}
