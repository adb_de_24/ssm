package com.ssm.sty.dao;

import com.ssm.sty.bean.User;
import com.ssm.sty.bean.UserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {
    long countByExample(UserExample example);
    int deleteByExample(UserExample example);
    int deleteByPrimaryKey(Integer id);
    int insert(User record);
    int insertSelective(User record);
    List<User> selectByExample(UserExample example);
    User selectByPrimaryKey(Integer id);
    List<User> selectByExampleWithDept(UserExample example);
    User selectByPrimaryKeyWithDept(Integer id);
    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);
    int updateByExample(@Param("record") User record, @Param("example") UserExample example);
    int updateByPrimaryKeySelective(User record);
    int updateByPrimaryKey(User record);
    //批量删除
    int batchDelUser(User record);
    //单个删除
    int oneDelUser(User user);
}